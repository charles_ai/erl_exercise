-module(e39).

%% 要用到对List的操作
%% 遍历一个List找出其中每个元素的出现次数

traverse([H|T], []=Db) ->
  write(H, Db),
  traverse(T, Db);
traverse([], Db) -> Db.

