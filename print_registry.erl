-module(print_registry).

%% to run this program
%%  > c(print_registry).
%%  > print_registry:install_code().
%%  > print_registry:run().

%%#doc maintainers(["joe@cslab.ericsson.se (Joe Armstrong)"]).
%%#doc title("Print the registry on a Window95/NT machine").
%%#doc keywords(["windows","registry"]).

-export([install_code/0, run/0, internal/0]).

-import(lists, [foreach/2, map/2]).
-import(win32reg, [open/1, current_key/1, sub_keys/1, close/1, 
    change_key/2, values/1]).

%% install code installs this program on my
%% Windows-NT machine

install_code() -> install_code(?MODULE).

install_code(Mod) ->
  {ok, Bin} = file:read_file(atom_to_list(Mod) ++ ".jam"),
  rpc:call(x@yuck, code, load_binary,  [Mod, "print_registry.jam", Bin]).

%% run() runs print_registry:go() on the remote machine

run() -> rpc:call(x@yuck, print_registry, internal, []).

keys() ->
  ["\\hkey_classes_root", "\\hkey_current_user",
    "\\hkey_local_machine","\\hkey_users","\\hkey_performance_data",
    "\\hkey_current_config","\\hkey_dyn_data"].

internal() ->
  {ok, Reg} = open([read]),
  map(fun(I) ->
        P = current_key(Reg),
        case change_key(Reg, I) of
          ok ->
            case sub_keys(Reg) of
              {ok, L1} ->
                io:format("Key(~s) has ~p subkeys~n",
                  [I, length(L1)]);
              Other ->
                io:format("Oops ~p~n",[Other])
            end;
          {error, _} ->
            io:format("No key ~p on this machine ~n",[I])
        end
    end, keys()),
  Key = "\\hkey_current_user\\software",
  list_keys(0, Reg, Key),
  close(Reg).

list_keys(Tab, Reg, Key) ->
  tab(Tab),
  io:format("~p", [Key]),
  change_key(Reg, Key),
  case sub_keys(Reg) of
    {ok, Vals} ->
      %% Remove the Microsoft key from the registry
      %% because this gives *lots* of output and this is supposed
      %% to be a *small* example
      Vals1 = lists:delete("Microsoft", Vals),
      foreach(fun(K) ->
            io:nl(),
            list_keys(Tab+2, Reg, K),
            case values(Reg) of
              {ok, []} ->
                io:nl();
              {ok, KVs} ->
                io:nl(),
                foreach(fun({KK,VV}) ->
                      tab(Tab+4),
                      io:format("~p = ~p~n",
                        [KK, VV])
                  end, KVs);
              _ ->
                true
            end,
            change_key(Reg, "..")
        end, Vals1);
    {error, _} ->
      true
  end.

tab(N) ->
  io:format("~s", [lists:duplicate(N, $ )]).
