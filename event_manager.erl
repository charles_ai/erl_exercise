-module(event_manager).
-export([start/2, stop/1]).
-export([add_handler/3, delete_handler/2, get_data/2, send_event/2]).
-export([init/2]).

start(Name, HandlerList) ->
  register(Name, spawn(event_manager,init,[HandlerList])),
  ok.

init(HandlerList) ->
  loop(initialize(HandlerList)).

initialize([]) -> [];
initialize([{Hanlder, InitData}!Rest]) ->
  [{Handler, Handler:init(InitData)}|initialize(Rest)].

