-module(e33).
-export([print/1, print_even/1]).

print(N) when N > 0 ->
  print(N,0).

print(N,M) when M < N ->
  X=M+1,io:format("Number:~p~n",[X]),print(N,X);
print(_,_) ->
  ok.

print_even(N) when is_integer(N) ->
  print_even(N, 0).

print_even(N,M) when M < N and (M rem 2 == 0) ->
  io:format("Number:~p~n",[M]),
  print_even(N,M+1);
print_even(N,M) when M < N ->
  print_even(N,M+1);
print_even(_,_) ->
  ok.
