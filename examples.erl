-module(examples).
-export([even/1,number/1]).

even(Int) when Int rem 2 == 0 ->true;
even(Int) when Int rem 2 =/= 0 -> false.

number(Num) when is_integer(Num) -> integer;
number(Num) when is_float(Num) -> float;
number(Other) -> false.
