-module(recursion).
-export([member/2]).

member(_,[]) -> false;
member(H, [H|T]) -> true;
member(H, [_|T]) -> member(H,T).
