-module(test).
-export([fac/1,double/1,check_num/1]).

fac(0) ->
  1;
fac(N) -> 
  N * fac(N-1).
double(X) ->
  times(X,2).
times(X,N) ->
  X * N.
check_num(X) ->
  case X rem 2 of 
    1 -> odd;
    _Other -> even
  end.
