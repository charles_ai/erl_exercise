-module(allocator).
-export([top_recover_alloc/3]).

top_recover_alloc([], Allocated, Pid) ->
  Pid ! no,
  top_recover([], Allocated);

top_recover_alloc([Resource|Free], Allocated, Pid) ->
  %% No need to unlink.
  Pid ! {yes, Resource},
  link(Pid),
  top_recover(Free, [{Resource, Pid}|Allocated]).

top_recover(Free, Allocated) ->
  receive
    {Pid, alloc} ->
      top_recover_alloc(Free, Allocated, Pid);
    {Pid, {release, Resource}} ->
      unlink(Pid),
      Allocated1 = delete({Resource, Pid}, Allocated),
      top_recover([Resource|Free], Allocated1);
    {'EXIT', Pid, Reason} ->
      %% No need to unlink.
      Resource = lookup(Pid, Allocated),
      Allocated1 = delete({Resource, Pid}, Allocated),
      top_recover([Resource|Free],Allocated1)
  end.

delete(H, [H|T]) ->
  T;
delete(X, [H|T]) ->
  [H|delete(X, T)].

lookup(Pid, [{Resource, Pid}|_]) ->
  Resource;
lookup(Pid, [_|Allocated]) ->
  lookup(Pid, Allocated).

