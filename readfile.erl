-module(readfile).
-export([read/1, file2lines/1]).

read(File) ->
  case file:read_line(File) of
    {ok, Data} -> [Data | read(File)];
    eof -> []
  end.

file2lines(File) ->
  {ok, Bin} = file:read_file(File),
  string2lines(binary_to_list(Bin), []).

string2lines("\n" ++ Str, Acc) -> [lists:reverse([$\n|Acc]) | string2lines(Str,[])];
string2lines([H|T], Acc)       -> string2lines(T, [H|Acc]);
string2lines([], Acc)          -> [lists:reverse(Acc)].
