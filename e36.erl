-module(e36).

-export([quicksort/1, sort/1]).

quicksort([]) -> [];
quicksort([P|T]) ->
  [L, H] = split(P, T, [], []),
  quicksort(L) ++ [P] ++ quicksort(H).

split(P, [F|T], L, H) when F < P -> split(P, T, [F|L], H);
split(P, [F|T], L, H) -> split(P, T, L, [F|H]);
split(_, [], L, H) -> [L, H].

%% 排序外壳
sort([]) -> [];
sort([E]) -> [E];
sort(L) ->
  {L1, L2} = lists:split(round(length(L)/2), L),
  merge(sort(L1), sort(L2)).

%% 归并操作
merge([], L2) -> L2;
merge(L1, []) -> L1;
merge([H1|T1]=L1, [H2|T2]=L2) ->
  if H1<H2 -> [H1] ++ merge(T1, L2);
    true -> [H2] ++ merge(L1, T2)
  end.
