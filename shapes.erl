-module(shapes).
-import(math,[sqrt/1,pi/0]).

area({square, Side}) ->
  Side*Side;
area({triangle,A,B,C}) -> 
  S=(A+B+C)/2,
  math:sqrt(S*(S-A)*(S-B)*(S-C));
area({circle,Radius}) ->
  math:pi() * Radius * Radius;
area(_Other) -> 
  {error, invalid_object}.

